distMatrix <- function(metric, hashVecList){
    n <- length(hashVecList)
    distMat <- matrix(,nrow=n, ncol=n)
    distMat[1,1] <- 0
    for(i in 2:n){
        distMat[i,i] <- 0
        for(j in 1: (i-1)){
            distMat[i,j] <- metric(hashVecList[[i]],hashVecList[[j]])
            distMat[j,i] <- distMat[i,j]
        }
    }
    return(distMat)
}
# Computes distance matrix for a list under a given metric
# Args:
#   list of vectors
#   metric
# Returns:
#   Pairwise distance matrix under given metric
# meant to be used with non-standard metrics on vectors of
# unequal length. Would be better if could use dist in some way
# but that seems inaccessible because its speed relies on code
# written in C.

# setting diagonal entries equal to zero should make it run a tiny bit
# faster but more importantly allows it to handle things like the
# jaccard distance, which is undefined between the empty set and
# itself.